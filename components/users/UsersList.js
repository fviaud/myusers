import React from 'react';
import UserElement from './UserElement';

const UserList = ({users, addFavori}) => {
   return (
      <ul className="list-group">
       {users.map((u, index) => (<UserElement i={index} user = {u} addFavori={ addFavori }/>))} 
      </ul>
   )
}

export default UserList;