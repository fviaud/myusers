import * as axios from 'axios';

const apiUser = axios.create({ baseURL: "https://randomuser.me/api/"})

export const apiUserMap = (u) => ({
  img: u.picture.medium,
  nom: u.name.last,
  prenom: u.name.first,
})

export default  apiUser;