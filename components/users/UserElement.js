import React from 'react';

const UserElement = ({user,addFavori}) => {
   return (
      <li  className="list-group-item list-group-item-action">
        <img alt="" src={user.img} className="rounded-circle" />
        <span> {user.nom}</span>
        <div className="d-flex flex-row justify-content-end">
        <button className="btn btn-small btn-primary" onClick={() => { addFavori(user.nom) }}>Add</button>
        </div>
      </li>
   )
}

export default UserElement;