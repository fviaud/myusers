import React, { Component } from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import apiFirebase from './conf/api.firebase';

import apiUser,{ apiUserMap } from './conf/api.users';

import { Header,UsersList} from './components';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      favoris: [],
      nbUsers: 10,
    };
  }

componentDidMount() {
    apiUser.get( "?results=" + this.state.nbUsers)
      .then( response => response.data.results )
      .then(
       usersApi => {
        this.setState({
          users: usersApi.map(apiUserMap),
          isLoaded:true,
        })
      })
      .catch( 
        error => {
          this.setState({
            isLoaded: true,
            error
          })}
      )

      apiFirebase.get('favoris.json')
      .then( response => {
        let favoris = response.data ? response.data : [];
        this.updateFavori(favoris)
      })
      .catch(err => console.log(err));  
  }

  addFavori = nom => {
    const user = { ...this.state.users.find(u => u.nom === nom) };
    this.setState(state => ({
      favoris: [...this.state.favoris, user]
    }), this.saveFavoris);
  }

  saveFavoris = () => {
    apiFirebase.put('favoris.json', this.state.favoris);
  }

  updateFavori = (favoris) => {
    this.setState({
      favoris,
      loaded: this.state.favoris ? true : false
    })
  }

  render() {
    const { error, isLoaded, users ,favoris} = this.state;
    return (
        <Router>
         <div className="App d-flex flex-column">
          <Header />
          <Switch>
            <Route path="/users" render={(props) => {
              return (<UsersList users={users}
                addFavori={this.addFavori}
              />
              )
            }} />
           <Route path="/favoris" render={(props) => {
              return (<UsersList users={favoris}/>
              )
            }} />
            <Redirect to="/users" />
          </Switch>
        </div>
        </Router>
      )
  }
}

export default App;